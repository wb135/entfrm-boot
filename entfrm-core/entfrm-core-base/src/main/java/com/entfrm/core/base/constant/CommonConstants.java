package com.entfrm.core.base.constant;

/**
 * @author yong
 * @date 2020/2/1
 *
 */
public interface CommonConstants {
	/**
	 * header 中租户ID
	 */
	String DB_NAME = "entfrm";

	/**
	 * UTF-8 字符集
	 */
	String UTF8 = "UTF-8";

	/**
	 * 成功标记
	 */
	Integer SUCCESS = 0;
	/**
	 * 失败标记
	 */
	Integer FAIL = 1;

	/**
	 * 角色前缀
	 */
	String ROLE = "ROLE_";
}
